// Typewrite effect for header
var ele = document.querySelector("#app_name");
var start="";
var end ="Welcome to Euro 2020...";
var ptr=0;
var undef =undefined;

undef = setInterval(function c(){
if(ptr==22){
    clearInterval(undef);
}
    var char = end[ptr];
    start+=char;
    ele.textContent=start;
    ptr++;
},150);

// navigation bar

// Hide other contents to show homepage while loading the page
var hide_home =document.querySelector('.homepage');
hide_home.style.display='block';
var hide_stats =document.querySelector('.statistics');
hide_stats.style.display='none';
var hide_fantasy =document.querySelector('.fantasyleague');
hide_fantasy.style.display='none';
var hide_fixtures =document.querySelector('.fixture');
hide_fixtures.style.display='none';
var hide_score_prediction =document.querySelector('.predictor');
hide_score_prediction.style.display='none';


// Events when the navigation links are clicked

// homepage
home.addEventListener('click',function(){
    homepage.style.display='block';
    statistics.style.display='none';
    fantasyleague.style.display='none';
    fixture.style.display='none';
    predictor.style.display='none'; 
    });
// statspage
stats.addEventListener('click',function(){
    homepage.style.display='none';
    statistics.style.display='block';
    fantasyleague.style.display='none';
    fixture.style.display='none';
    predictor.style.display='none'; 

    });
// fantasy league
fantasy.addEventListener('click',function(){
    homepage.style.display='none';
    statistics.style.display='none';
    fantasyleague.style.display='flex';
    fixture.style.display='none';
    predictor.style.display='none'; 
    });
// fixtures
fixtures.addEventListener('click',function(){
    homepage.style.display='none';
    statistics.style.display='none';
    fantasyleague.style.display='none';
    fixture.style.display='flex';
    predictor.style.display='none'; 
    });
// score prediction
score_predictor.addEventListener('click',function(){
     homepage.style.display='none';
    statistics.style.display='none';
    fantasyleague.style.display='none';
    fixture.style.display='none';
    predictor.style.display='flex'; 
    });
