from flask import Flask,request,render_template
import json
import numpy as np
import pandas as pd
from scipy.stats import poisson,skellam
import statsmodels.api as sm
import statsmodels.formula.api as smf

app = Flask(__name__)
shooting = ["Shots on target","Shots on target %","Shots on target per 90 minutes","Shots per 90 minutes","Goal per shot","Goals per shot on target"]

def simulate_match(foot_model, homeTeam, awayTeam, max_goals=10):
  home_goals_avg = foot_model.predict(pd.DataFrame(data={'team': homeTeam, 
                                                            'opponent': awayTeam,'home':1},
                                                      index=[1])).values[0]
  away_goals_avg = foot_model.predict(pd.DataFrame(data={'team': awayTeam, 
                                                            'opponent': homeTeam,'home':0},
                                                      index=[1])).values[0]
  team_pred = [[poisson.pmf(i, team_avg) for i in range(0, max_goals+1)] for team_avg in [home_goals_avg, away_goals_avg]]
  return(np.outer(np.array(team_pred[0]), np.array(team_pred[1])))

@app.route('/', methods=['GET'])
@app.route('/#homepage', methods=['GET'])
def home():
	homeJsonObj = open("home.json", "r")
	homePageContent = homeJsonObj.read()
	homePage = json.loads(homePageContent)
	return render_template('index.html', homePageNews=homePage)

@app.route('/visual', methods=['GET'])
def teamstats():
	return render_template('teamstats.html')

@app.route('/', methods=['POST'])
def stats():
	if request.method == 'POST':
		euroStatForm = request.form
		SelectTeam = euroStatForm['select team']
		SortByKey = euroStatForm['sort by']
		positionKey = euroStatForm['position'] 
		euroRawDF = pd.read_csv('eurosquadselect.csv',encoding='latin-1')
		euroDF = euroRawDF.fillna(0)
		if positionKey != "All":
			euroDF = euroDF[positionKey == euroDF["Position"]]
		euroSelectedDF = euroDF[["Player","Team",SortByKey]]
		if SelectTeam == "All":
			euroSortedDF = euroSelectedDF.sort_values(by=[SortByKey], ascending=False)
		else :
			euroSelectedDF = euroSelectedDF[euroSelectedDF["Team"] == SelectTeam]
			euroSortedDF = euroSelectedDF.sort_values(by=[SortByKey], ascending=False)
		euroSortedDF = euroSelectedDF[euroSelectedDF[SortByKey] > 0]
		euroSortedDF = euroSelectedDF.sort_values(by=[SortByKey], ascending=False)
		euroSortedDF.rename(columns = {SortByKey:'Result'}, inplace = True)
		euroSortedJson = euroSortedDF.to_json(orient="records")
		euroStats = json.loads(euroSortedJson)
		return render_template('index.html', statsData=euroStats,Player="Player",Team="Team",sortby=SortByKey)

@app.route('/player', methods=['GET', 'POST'])
def playerstats():
	if request.method == 'POST':
		euroStatForm = request.form
		SelectTeam = euroStatForm['select team']
		SortByKey = euroStatForm['sort by']
		positionKey = euroStatForm['position'] 
		euroRawDF = pd.read_csv('playerxg.csv',encoding='latin-1')
		euroDF = euroRawDF.fillna(0)
		if positionKey != "All":
			euroDF = euroDF[positionKey == euroDF["Position"]]
		euroSelectedDF = euroDF[["Player","Team",SortByKey]]
		if SelectTeam == "All":
			euroSortedDF = euroSelectedDF.sort_values(by=[SortByKey], ascending=False)
		else :
			euroSelectedDF = euroSelectedDF[euroSelectedDF["Team"] == SelectTeam]
			euroSortedDF = euroSelectedDF.sort_values(by=[SortByKey], ascending=False)
		euroSortedDF.rename(columns = {SortByKey:'Result'}, inplace = True)
		euroSortedDF = euroSortedDF.head(30)
		euroSortedJson = euroSortedDF.to_json(orient="records")
		euroStats = json.loads(euroSortedJson)
		return render_template('player.html', statsData=euroStats,Player="Player",Team="Team",sortby=SortByKey)
	return render_template('player.html')

@app.route('/predict', methods=['GET', 'POST'])
def execute():
  if request.method == "POST":
        details = request.form
        hteam = details['home team']
        ateam = details['away team']
        if hteam == ateam:
        	return render_template('select.html',data="hidden")
        epl_1617 = pd.read_csv("euroqualifiers.csv",encoding='latin-1')
        epl_1617 = epl_1617[['HomeTeam','AwayTeam','HomeGoals','AwayGoals']]
        epl_1617 = epl_1617.dropna()
        goal_model_data = pd.concat([epl_1617[['HomeTeam','AwayTeam','HomeGoals']].assign(home=1).rename(
            columns={'HomeTeam':'team', 'AwayTeam':'opponent','HomeGoals':'goals'}),
           epl_1617[['AwayTeam','HomeTeam','AwayGoals']].assign(home=0).rename(
            columns={'AwayTeam':'team', 'HomeTeam':'opponent','AwayGoals':'goals'})])
        poisson_model = smf.glm(formula="goals ~ home + team + opponent", data=goal_model_data, 
                        family=sm.families.Poisson()).fit()
        poisson_model.summary()
        #home team goals
        hteamgoals1 = poisson_model.predict(pd.DataFrame(data={'team': hteam, 'opponent': ateam,
                                       'home':1},index=[1]))
        #away team goals
        ateamgoals1 = poisson_model.predict(pd.DataFrame(data={'team': ateam, 'opponent': hteam,
                                       'home':0},index=[1]))
        hteamgoals2 = poisson_model.predict(pd.DataFrame(data={'team': ateam, 'opponent': hteam,
                                       'home':1},index=[1]))
        #away team goals
        ateamgoals2 = poisson_model.predict(pd.DataFrame(data={'team': hteam, 'opponent': ateam,
                                       'home':0},index=[1]))
        #match probabillity
        prob = simulate_match(poisson_model, hteam, ateam, max_goals=10)
        win1 = np.sum(np.tril(prob, -1))*100
        draw1 = np.sum(np.diag(prob))*100
        loss1 = np.sum(np.triu(prob, 1))*100
        prob = simulate_match(poisson_model, ateam, hteam, max_goals=10)
        win2 = np.sum(np.tril(prob, -1))*100
        draw2 = np.sum(np.diag(prob))*100
        loss2 = np.sum(np.triu(prob, 1))*100
        return render_template('select.html',data="none",home=hteam,away=ateam,homescore=int(round((hteamgoals1+ateamgoals2)/2)),awayscore=int(round((hteamgoals2+ateamgoals1)/2)),wp=int(round((win1+loss2)/2)),lp=int(round((win2+loss1)/2)),dp=int(round((draw1+draw2)/2)))
  return render_template('select.html',data="hidden")	

if __name__ == '__main__':
    app.run()